# soundshare-webapp

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=soundflow1_soundshare-webapp&metric=alert_status)](https://sonarcloud.io/dashboard?id=soundflow1_soundshare-webapp)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=soundflow1_soundshare-webapp&metric=bugs)](https://sonarcloud.io/dashboard?id=soundflow1_soundshare-webapp)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=soundflow1_soundshare-webapp&metric=code_smells)](https://sonarcloud.io/dashboard?id=soundflow1_soundshare-webapp)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=soundflow1_soundshare-webapp&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=soundflow1_soundshare-webapp)


### Technology Stack

Component         | Technology
---               | ---
Frontend          | [Vue 4.2.3](https://github.com/vuejs/vue) (Typescript)
Security          | Token Based (Keycloak and [JWT](https://github.com/auth0/java-jwt) )
Client Build Tools| [vue-cli](https://github.com/vuejs/vue-cli), Webpack, npm


### Accessing Application
Component         | URL                                      
---               | ---                                      
Frontend          |  http://localhost:4200                   