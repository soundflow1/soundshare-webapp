import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "./css/main.scss";
import "./css/button.scss";
import "./css/text-input.scss";
import "./css/select.scss";
import "./css/lists.scss";
import { KeycloakService } from "./ts/Auth";
import "bootstrap/dist/css/bootstrap.css";
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { NavbarPlugin } from "bootstrap-vue";
import { EventBus } from "@/ts/Eventbus";
import VueAxios from 'vue-axios'
import axios from 'axios'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Config from "./ts/Config";

Vue.config.productionTip = false;
Vue.use(NavbarPlugin);
Vue.use(VueAxios, axios);
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.axios.defaults.baseURL = Config.baseUrl;

new Vue({
router,
render: h => h(App),
    data: {
        results: []
    }
}).$mount("#app");

KeycloakService.keycloak.onAuthSuccess = () => {
  EventBus.$emit("auth-success");
};

KeycloakService.init()
  .then(auth => {
    if (!auth) {
      console.log("Unauthenticated");
      router.push("/");
    } else {
      console.log("Authenticated");
    }

    if (KeycloakService.keycloak.token != null) {
      localStorage.setItem("vue-token", KeycloakService.keycloak.token);
    }
    if (KeycloakService.keycloak.refreshToken != null) {
      localStorage.setItem(
        "vue-refresh-token",
        KeycloakService.keycloak.refreshToken
      );
    }

    setTimeout(() => {
      KeycloakService.keycloak
        .updateToken(70)
        .then(refreshed => {
          if (refreshed) {
            console.log("Token refreshed" + refreshed);
          } else {
            console.log("Token not refreshed, valid for");
          }
        })
        .catch(() => {
          console.log("Failed to refresh tokend");
        });
    }, 60000);
  })
  .catch(() => {
    console.log("Authenticated Failed");
  });
