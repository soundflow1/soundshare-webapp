import Vue from "vue";
import VueRouter from "vue-router";
import LoginScreen from "../views/LoginScreen.vue";

Vue.use(VueRouter);

export const routes: any = [
  {
    path: "/",
    name: "Login",
    component: LoginScreen
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/home",
    name: "Home",
    component: () => import(/* webpackChunkName: "home" */ "../views/Home.vue")
  },
  {
    path: "/upload",
    name: "Upload",
    component: () =>
      import(/* webpackChunkName: "home" */ "../views/Upload.vue")
  },
  {
    path: "/Library",
    name: "Library",
    component: () =>
        import(/* webpackChunkName: "home" */ "../views/Library.vue")
  },
  {
    path: "/Stream/:id",
    name: "Stream",
    component: () =>
        import(/* webpackChunkName: "home" */ "../views/Stream.vue"),
    props: true
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
