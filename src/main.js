import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "./css/main.scss";
import "./css/button.scss";
import "./css/text-input.scss";
import "./css/select.scss";
import "./css/lists.scss";
Vue.config.productionTip = false;
new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
//# sourceMappingURL=main.js.map
