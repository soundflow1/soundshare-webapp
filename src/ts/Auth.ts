// eslint-disable-next-line
import Keycloak from "keycloak-js";
import Config from "./Config";
import { EventBus } from "@/ts/Eventbus";

export class KeycloakService {

  static keycloak = Keycloak({
    realm: Config.keycloakConfig.realm,
    url: Config.keycloakConfig.url,
    clientId: Config.keycloakConfig.clientId
  });
  private static username: string | undefined;

  static init() {
    return KeycloakService.keycloak.init({
      onLoad: "check-sso",
      checkLoginIframe: false
    });
  }
  static login(redirecturi: any) {
    KeycloakService.keycloak.login(redirecturi);
  }

  static logout() {
    KeycloakService.keycloak.logout();
  }

  static register() {
    KeycloakService.keycloak.register();
  }

  static callWhenAuth(func: any): void {
    if (KeycloakService.keycloak.authenticated) {
      func();
    } else {
      EventBus.$on("auth-success", func);
    }
  }

  static async getUsername() {
    if (this.username == null) {
      await KeycloakService.keycloak.loadUserProfile().then(function (profile) {
        KeycloakService.username = profile.username
      })
    }
      return this.username;
  }

  static isAuthenticated() {
    return KeycloakService.keycloak.authenticated;
  }

}
