// eslint-disable-next-line
import Keycloak from "keycloak-js";
import Config from "./Config";
import EventBus from "./EventBus";
const keycloak = Keycloak({
  url: Config.keycloakConfig.url,
  realm: Config.keycloakConfig.realm,
  clientId: Config.keycloakConfig.clientId
});
export { keycloak };
export function callWhenAuth(func) {
  if (keycloak.authenticated) {
    func();
  } else {
    EventBus.$on("auth-success", func);
  }
}
export function isAdmin() {
  return keycloak.hasRealmRole("admin");
}
//# sourceMappingURL=Auth.js.map
