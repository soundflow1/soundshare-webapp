const stagingConfig = {
  apiHost: "",
  baseUrl: "/v2",
  keycloakConfig: {
    url: "",
    realm: "soundshare",
    clientId: "webapp",
    redirectUri: "",
    logOutUri: ""
  }
};

// const testConfig = {
//   apiHost: "http://localhost:7080",
//   baseUrl: "http://localhost:7700",
//   keycloakConfig: {
//     url: "http://localhost:8080/auth",
//     realm: "soundflow",
//     clientId: "webapp",
//     redirectUri: "http://localhost:9000/home",
//     logOutUri: "http://localhost:9000/"
//   }
// };

const productionConfig = {
  apiHost: "http://api.soundflow.social",
  baseUrl: "http://api.soundflow.social",
  keycloakConfig: {
    url: "http://identity.soundflow.social",
    realm: "soundflow",
    clientId: "webapp",
    redirectUri: "http://soundflow.social/home",
    logOutUri: "http://soundflow.social/"
  }
};

let config = productionConfig;

// eslint-disable-next-line
const type: string = "production";
switch (type) {
  case "production":
    config = productionConfig;
    break;
  case "staging":
    config = stagingConfig;
    break;
  case "test":
    config = productionConfig;
    break;
  default:
    config = productionConfig;
    break;
}

export default config;
